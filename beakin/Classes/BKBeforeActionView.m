//
//  BKBeforeActionView.m
//  beakin
//
//  Created by Jonathan Araujo-Levy on 04/12/12.
//  Copyright (c) 2012 coesense. All rights reserved.
//

#import "BKBeforeActionView.h"
#import "BKActionViewController.h"

@interface BKBeforeActionView () {
    SLComposeViewController *BKActionSocial;
}

@end

@implementation BKBeforeActionView 

@synthesize parentViewController;

@synthesize BKBGActionImage;
@synthesize BKActionCatName;
@synthesize BKActionSentenceLike;
@synthesize BKActionSentenceDislike;
@synthesize BKActionDislikeSound;
@synthesize BKActionLikeSound;

- (id)initWithFrame:(CGRect)frame andActionData:(NSMutableArray *)actionData
{
    
    //Settings
    self.parentViewController = [actionData objectAtIndex:0];
    self.BKBGActionImage      = [actionData objectAtIndex:1];
    self.BKActionCatName      = [actionData objectAtIndex:2];
    self.BKActionSentenceLike = [actionData objectAtIndex:3];
    self.BKActionSentenceDislike = [actionData objectAtIndex:4];
    self.BKActionLikeSound    = [actionData objectAtIndex:5];
    self.BKActionDislikeSound = [actionData objectAtIndex:6];
    BKActionSocial            = [actionData objectAtIndex:7];
    
    self = [super initWithFrame:frame];
    if (self) {
        
        [self beforeActionUILoading];
    }
    
    return self;
}

- (void) beforeActionUILoading {
    // BackgroundView
    NSString *BKActionBGPath = BKBGActionImage;
    
    UIImage *BKActionBGImage = [UIImage imageNamed:BKActionBGPath];
    UIImageView *BKActionBGView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [BKActionBGView setImage:BKActionBGImage];
    [self addSubview:BKActionBGView];
    
    
    // Describe View
    UIImage *motionActionImageDescribe = [UIImage imageNamed:@"BK_motionActionView.png"];
    UIImageView *motionActionViewDescribe = [[UIImageView alloc] initWithFrame:CGRectMake(
                                                                                          (self.frame.size.width/2)-(motionActionImageDescribe.size.width/2),
                                                                                          (self.frame.size.height/2)-(motionActionImageDescribe.size.height/1.2),
                                                                                          motionActionImageDescribe.size.width,
                                                                                          motionActionImageDescribe.size.height)];
    [motionActionViewDescribe setImage:motionActionImageDescribe];
    [self addSubview:motionActionViewDescribe];
    
    // Share FB Button
    UIButton *BKFBShareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *BKFBShareButtonImageNormal = [UIImage imageNamed:@"BK_FBShareNormal.png"];
    UIImage *BKFBShareButtonImageHighlighted = [UIImage imageNamed:@"BK_FBShareHighlighted.png"];
    [BKFBShareButton setFrame:CGRectMake((self.frame.size.width/2)-(BKFBShareButtonImageNormal.size.width/2),
                                         (motionActionViewDescribe.frame.origin.y+motionActionViewDescribe.frame.size.height+45),
                                         BKFBShareButtonImageNormal.size.width, BKFBShareButtonImageNormal.size.height)];
    [BKFBShareButton setBackgroundImage:BKFBShareButtonImageNormal forState:UIControlStateNormal];
    [BKFBShareButton setBackgroundImage:BKFBShareButtonImageHighlighted forState:UIControlStateHighlighted];
    [BKFBShareButton addTarget:self action:@selector(FacebookSharing) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:BKFBShareButton];
}


// Facebook Sharing
- (void) FacebookSharing {
    [BKActionSocial setInitialText:self.BKActionSentenceLike];
    [self.parentViewController presentViewController:BKActionSocial animated:YES completion:nil];
}



@end
