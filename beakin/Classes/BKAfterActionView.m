//
//  BKAfterActionView.m
//  beakin
//
//  Created by Jonathan Araujo-Levy on 04/12/12.
//  Copyright (c) 2012 coesense. All rights reserved.
//

#import "BKAfterActionView.h"

@interface BKAfterActionView () {
    int userPersonality;
}
@end

@implementation BKAfterActionView

@synthesize sharedAppDelegate;

- (id)initWithFrame:(CGRect)frame
{
    // appDelegate
    self.sharedAppDelegate = [[UIApplication sharedApplication] delegate];
    
    self = [super initWithFrame:frame];
    if (self) {
        [self afterActionUILoading];
    }
    return self;
}

- (void) afterActionUILoading {
    // BackgroundView
    NSString *BKActionBGPath = @"BK_actionFallbackView.jpg";
    UIImage *BKActionBGImage = [UIImage imageNamed:BKActionBGPath];
    UIImageView *BKActionBGView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [BKActionBGView setImage:BKActionBGImage];
    [self addSubview:BKActionBGView];
    
    // HeadLabel
    UILabel *headerText = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, self.frame.size.width, 50)];
    [headerText setBackgroundColor:[UIColor clearColor]];
    [headerText setTextAlignment:NSTextAlignmentCenter];
    [headerText setFont:[UIFont fontWithName:@"Helvetica Neue" size:26.f]];
    [headerText setTextColor:[UIColor colorWithRed:(217/255.f) green:(217/255.f) blue:(217/255.f) alpha:1]];
    [headerText setText:@"YOU ARE ..."];
    [self addSubview:headerText];
    
    
    int valueLike = [self.sharedAppDelegate.userStackLike intValue];
    int valueDislike = [self.sharedAppDelegate.userStackDislike intValue];
    
    if(valueDislike > valueLike){
        // Wicked Badge
        UIImage *BKWickedBadgeImage = [UIImage imageNamed:@"BK_actionWickedBadge.png"];
        UIImageView *BKWickedBadgeImageView = [[UIImageView alloc] initWithFrame:CGRectMake((self.frame.size.width/2)-(BKWickedBadgeImage.size.width/2), 85, BKWickedBadgeImage.size.width, BKWickedBadgeImage.size.height)];
        [BKWickedBadgeImageView setImage:BKWickedBadgeImage];
        [self addSubview:BKWickedBadgeImageView];
    }else{
        // Cheeky Badge
        UIImage *BKCheekyBadgeImage = [UIImage imageNamed:@"BK_actionCheekyBadge.png"];
        UIImageView *BKCheekyBadgeImageView = [[UIImageView alloc] initWithFrame:CGRectMake((self.frame.size.width/2)-(BKCheekyBadgeImage.size.width/2), 85, BKCheekyBadgeImage.size.width, BKCheekyBadgeImage.size.height)];
        [BKCheekyBadgeImageView setImage:BKCheekyBadgeImage];
        [self addSubview:BKCheekyBadgeImageView];
    }
    
    int rand = arc4random() % 10;
    if(rand > 5){
        //cheat
        int value = [self.sharedAppDelegate.userStackDislike intValue];
        self.sharedAppDelegate.userStackDislike = [NSNumber numberWithInt:value + 1];
    }else{
        //cheat
        int value = [self.sharedAppDelegate.userStackLike intValue];
        self.sharedAppDelegate.userStackLike = [NSNumber numberWithInt:value + 1];
    }
    
    // LikeResults
    UILabel *likeResultNumber = [[UILabel alloc] initWithFrame:CGRectMake(35, 280, 100, 50)];
    [likeResultNumber setBackgroundColor:[UIColor clearColor]];
    [likeResultNumber setTextAlignment:NSTextAlignmentCenter];
    [likeResultNumber setFont:[UIFont fontWithName:@"Helvetica Neue" size:44.f]];
    [likeResultNumber setTextColor:[UIColor colorWithRed:(50/255.f) green:(50/255.f) blue:(50/255.f) alpha:1]];
    
    [likeResultNumber setText:[NSString stringWithFormat:@"%@",self.sharedAppDelegate.userStackLike]];
    
    UILabel *likeTitle = [[UILabel alloc] initWithFrame:CGRectMake(likeResultNumber.frame.origin.x, likeResultNumber.frame.origin.y+30, 100, 50)];
    [likeTitle setBackgroundColor:[UIColor clearColor]];
    [likeTitle setTextAlignment:NSTextAlignmentCenter];
    [likeTitle setFont:[UIFont fontWithName:@"Helvetica Neue" size:26.f]];
    [likeTitle setTextColor:[UIColor whiteColor]];
    [likeTitle setText:@"LIKE"];
    
    [self addSubview:likeTitle];
    [self addSubview:likeResultNumber];
    
    // DislikeResults
    UILabel *DislikeResultNumber = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width-135, 280, 100, 50)];
    [DislikeResultNumber setBackgroundColor:[UIColor clearColor]];
    [DislikeResultNumber setTextAlignment:NSTextAlignmentCenter];
    [DislikeResultNumber setFont:[UIFont fontWithName:@"Helvetica Neue" size:44.f]];
    [DislikeResultNumber setTextColor:[UIColor colorWithRed:(50/255.f) green:(50/255.f) blue:(50/255.f) alpha:1]];
    [DislikeResultNumber setText:[NSString stringWithFormat:@"%@",self.sharedAppDelegate.userStackDislike]];
    
    UILabel *DislikeTitle = [[UILabel alloc] initWithFrame:CGRectMake(DislikeResultNumber.frame.origin.x-25, DislikeResultNumber.frame.origin.y+30, 150, 50)];
    [DislikeTitle setBackgroundColor:[UIColor clearColor]];
    [DislikeTitle setTextAlignment:NSTextAlignmentCenter];
    [DislikeTitle setFont:[UIFont fontWithName:@"Helvetica Neue" size:26.f]];
    [DislikeTitle setTextColor:[UIColor whiteColor]];
    [DislikeTitle setText:@"DISLIKE"];
    
    [self addSubview:DislikeTitle];
    [self addSubview:DislikeResultNumber];
    
}


@end
