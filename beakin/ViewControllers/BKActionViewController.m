//
//  BKActionViewController.m
//  beakin
//
//  Created by Jonathan Araujo-Levy on 04/12/12.
//  Copyright (c) 2012 coesense. All rights reserved.
//

#import "BKActionViewController.h"
#import "BKBeforeActionView.h"
#import "BKAfterActionView.h"
#import <AudioToolbox/AudioToolbox.h>

@interface BKActionViewController (){
    SLComposeViewController *socialViewController;
    NSMutableArray *BKDataArray;
    NSDictionary *BKTransitionArray;
    BOOL isHorizontal;
    SystemSoundID audioEffect;
}

@end

@implementation BKActionViewController

//@synthesize sharedAppDelegate;

- (id)initWithData:(NSDictionary *)BKActionData
{
    
    // Delegate
    //self.sharedAppDelegate = [[UIApplication sharedApplication] delegate];
    
    // Audio Settings
    AudioServicesDisposeSystemSoundID(audioEffect);
    
    // Position BOOL
    isHorizontal = NO;
    
    // Getting Data
    BKTransitionArray = [[NSDictionary alloc] init];
    BKTransitionArray = BKActionData;
    
    // Setting Accelerometer
    [[UIAccelerometer sharedAccelerometer] setUpdateInterval:(1.0 / 14)];
    [[UIAccelerometer sharedAccelerometer] setDelegate:self];
    
    self = [super init];
    if (self) {}
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // InitView
    [self initElements];
    [self actionUILoading];
    [self setBackButton];
}

- (void) initElements {
    // Social
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]){
        socialViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        SLComposeViewControllerCompletionHandler __block completionHandler = ^(SLComposeViewControllerResult result)
        {
            [socialViewController dismissViewControllerAnimated:YES completion:nil]; // Opening facebook shit as modal view
            
            if(result == SLComposeViewControllerResultCancelled){}
            else if (result == SLComposeViewControllerResultDone){
                BKAfterActionView *afterActionView = [[BKAfterActionView alloc] initWithFrame:CGRectMake(0, 44, self.view.frame.size.width,
                                                                                                         self.view.frame.size.height)];                
                [self.view addSubview:afterActionView];
            }else {}
        };
        [socialViewController setCompletionHandler:completionHandler];
        
    }else{ NSLog(@"NO"); }
    
    // Filling Main Array
    BKDataArray = [[NSMutableArray alloc] init];
    [BKDataArray addObject:self];
    [BKDataArray addObject:[NSString stringWithFormat:@"%@",[BKTransitionArray objectForKey:@"main_img"]]];
    [BKDataArray addObject:[NSString stringWithFormat:@"%@",[BKTransitionArray objectForKey:@"name"]]];
    [BKDataArray addObject:[NSString stringWithFormat:@"%@",[[[BKTransitionArray objectForKey:@"judgements"] objectAtIndex:0] objectForKey:@"sentence"]]];
    [BKDataArray addObject:[NSString stringWithFormat:@"%@",[[[BKTransitionArray objectForKey:@"judgements"] objectAtIndex:1] objectForKey:@"sentence"]]];
    [BKDataArray addObject:[NSString stringWithFormat:@"%@",[[[BKTransitionArray objectForKey:@"judgements"] objectAtIndex:0] objectForKey:@"tone"]]];
    [BKDataArray addObject:[NSString stringWithFormat:@"%@",[[[BKTransitionArray objectForKey:@"judgements"] objectAtIndex:1] objectForKey:@"tone"]]];
    [BKDataArray addObject:socialViewController];
}

// UILoading
- (void) actionUILoading {
    
    BKBeforeActionView *beforeActionView = [[BKBeforeActionView alloc] initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, self.view.frame.size.height)  andActionData:BKDataArray];
    [self.view addSubview:beforeActionView];
    
}

// Accelero
- (void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
    
    // detecting Actions
    float roundX = floor(acceleration.x*10);
    
    if (roundX == -11 || roundX == -10) { isHorizontal = YES; }
    
    if(isHorizontal){
        if(roundX == -8 || roundX == -7){ [self motionDislikeHandler]; }
        else if (roundX == -5 || roundX == -4){ [self motionLikeHandler]; }
    }
}



// Handeling motionDislikeEvent

- (void) motionDislikeHandler {
    [[UIAccelerometer sharedAccelerometer] setDelegate:nil];
    
    // playSound
    NSString *BKDislikeSoundPath = [BKDataArray objectAtIndex:6];
    [self playSound:[BKDislikeSoundPath substringToIndex:[BKDislikeSoundPath length]-4]];
    
    // userData
    //int value = [self.sharedAppDelegate.userStackDislike intValue];
    //self.sharedAppDelegate.userStackDislike = [NSNumber numberWithInt:value + 1];
    
    // SocialShare
    [self FacebookSharing:[BKDataArray objectAtIndex:4]];
}

// Handeling motionLikeEvent

- (void) motionLikeHandler {
    [[UIAccelerometer sharedAccelerometer] setDelegate:nil];
    
    // playSound
    NSString *BKLikeSoundPath = [BKDataArray objectAtIndex:5];
    [self playSound:[BKLikeSoundPath substringToIndex:[BKLikeSoundPath length]-4]];
    
    // userData
    //int value = [self.sharedAppDelegate.userStackLike intValue];
    //self.sharedAppDelegate.userStackLike = [NSNumber numberWithInt:value + 1];
    
    // SocialShare
    [self FacebookSharing:[BKDataArray objectAtIndex:3]];
}

// Facebook Sharing
- (void) FacebookSharing:(NSString *)Sentence {
    [socialViewController setInitialText:Sentence];
    [self presentViewController:socialViewController animated:YES completion:nil];
}

-(void) playSound:(NSString *)fName
{
    NSString *path  = [[NSBundle mainBundle] pathForResource:fName ofType:@"mp3"];
    if ([[NSFileManager defaultManager] fileExistsAtPath : path])
    {
        NSURL *pathURL = [NSURL fileURLWithPath : path];
        AudioServicesCreateSystemSoundID((CFURLRef) CFBridgingRetain(pathURL), &audioEffect);
        AudioServicesPlaySystemSound(audioEffect);
    }
    else
    {
        NSLog(@"error, file not found: %@", path);
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
