//
//  BKInfoViewController.m
//  beakin
//
//  Created by Jonathan Araujo-Levy on 04/12/12.
//  Copyright (c) 2012 coesense. All rights reserved.
//

#import "BKInfoViewController.h"
#import <AudioToolbox/AudioToolbox.h>

@interface BKInfoViewController (){
    BOOL slideBeingUsed;
    CGFloat lastContentOffset;
    int lastViewedSlide;
    SystemSoundID audioEffect;
    BOOL isHorizontal;
}

@end

@implementation BKInfoViewController

@synthesize BKSliderInfo;
@synthesize BKSliderDataArray;
@synthesize BKSliderPaging;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    // Audio Settings
    AudioServicesDisposeSystemSoundID(audioEffect);
    
    // Setting Accelerometer
    [[UIAccelerometer sharedAccelerometer] setUpdateInterval:(1.0 / 14)];
    [[UIAccelerometer sharedAccelerometer] setDelegate:self];
    
    // Position BOOL
    isHorizontal = NO;
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initElements];
    [self infoUILoading];
}

- (void) initElements {
    self.BKSliderDataArray = [[NSMutableArray alloc] init];
    
    NSArray *firstSlide = [NSArray  arrayWithObjects:[NSString stringWithFormat:@"Need help ? Wanna share your views in a different way ? « Beakin' » allows you to give your opinion about everything in your life."],
                           [UIImage imageNamed:@"BK_FirstSlide.png"], nil];
    [self.BKSliderDataArray insertObject:firstSlide atIndex:0];
    NSArray *secondSlide = [NSArray  arrayWithObjects:[NSString stringWithFormat:@"How ? Just pick the category of the thing you want to react about..."],
                            [UIImage imageNamed:@"BK_SecondSlide.png"], nil];
    [self.BKSliderDataArray insertObject:secondSlide atIndex:1];
    NSArray *thirdSlide = [NSArray  arrayWithObjects:[NSString stringWithFormat:@"Then turn you iphone up to like something, or down to dislike it. The stronger the motion, the louder the feedback... And don't forget to share it with your friends."],
                           [UIImage imageNamed:@"BK_ThirdSlide.png"], nil];
    [self.BKSliderDataArray insertObject:thirdSlide atIndex:2];
}


- (void) infoUILoading {
    
    // CloseButton
    UIButton *BKCloseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *BKCloseButtonImageNormal = [UIImage imageNamed:@"BK_closeButtonNormal.png"];
    UIImage *BKCloseButtonImageHighlighted = [UIImage imageNamed:@"BK_closeButtonHighlighted.png"];
    [BKCloseButton setFrame:CGRectMake(0 ,0,BKCloseButtonImageNormal.size.width, BKCloseButtonImageNormal.size.height)];
    [BKCloseButton setBackgroundImage:BKCloseButtonImageNormal forState:UIControlStateNormal];
    [BKCloseButton setBackgroundImage:BKCloseButtonImageHighlighted forState:UIControlStateHighlighted];
    [BKCloseButton addTarget:self action:@selector(dismissCurrentView) forControlEvents:UIControlEventTouchUpInside];
    [self.BKNavgiationBar addSubview:BKCloseButton];
    
    // SliderBackground
    UIImageView *BKSliderBGView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, self.view.frame.size.height)];
    [BKSliderBGView setImage:[UIImage imageNamed:@"BK_actionFallbackView.jpg"]];
    [self.view addSubview:BKSliderBGView];
    
    // Slider
    self.BKSliderInfo = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, BKSliderBGView.frame.size.height)];
    [self.BKSliderInfo setDelegate:self];
    [self.BKSliderInfo setBackgroundColor:[UIColor clearColor]];
    [self.BKSliderInfo setContentSize:CGSizeMake(self.BKSliderInfo.frame.size.width * [self.BKSliderDataArray count], self.BKSliderInfo.frame.size.height)];
    [self.BKSliderInfo setShowsHorizontalScrollIndicator:NO];
    [self.BKSliderInfo setPagingEnabled:YES];
    [self.BKSliderInfo setCanCancelContentTouches:NO];
    [self.BKSliderInfo setAlwaysBounceHorizontal:NO];
    [self.view addSubview:self.BKSliderInfo];
    
    // SliderPageControl
    self.BKSliderPaging = [[UIPageControl alloc] initWithFrame:CGRectMake(0,
                                                                          self.BKSliderInfo.frame.size.height-40,
                                                                          self.view.frame.size.width,30)];
    [self.BKSliderPaging setBackgroundColor:[UIColor clearColor]];
    [self.BKSliderPaging setNumberOfPages:[self.BKSliderDataArray count]];
    [self.BKSliderPaging setCurrentPage:0];
    [self.BKSliderPaging setPageIndicatorTintColor:[UIColor colorWithRed:(54/255.f) green:(54/255.f) blue:(54/255.f) alpha:1]];
    [self.BKSliderPaging setCurrentPageIndicatorTintColor:[UIColor colorWithRed:(217/255.f) green:(217/255.f) blue:(217/255.f) alpha:1]];
    [self.BKSliderPaging addTarget:self action:@selector(PageControlChanged) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.BKSliderPaging];
    
    for (int i = 0; i < self.BKSliderDataArray.count; i++) {
        UIView *BKSliderPage = [[UIView alloc] initWithFrame:CGRectMake((self.BKSliderInfo.frame.size.width * i),0
                                                                      ,self.BKSliderInfo.frame.size.width,self.BKSliderInfo.frame.size.height)];
        UIImageView *BKSliderImage = [[UIImageView alloc] initWithImage:[[self.BKSliderDataArray objectAtIndex:i] objectAtIndex:1]];
        CGRect frame = CGRectMake(BKSliderPage.frame.size.width/2-BKSliderImage.frame.size.width/2, 0, BKSliderImage.frame.size.width, BKSliderImage.frame.size.height);
        BKSliderImage.frame = frame;
        
        UILabel *BKSliderTitle = [[UILabel alloc] initWithFrame:CGRectMake((25+(self.BKSliderInfo.frame.size.width * 0)),
                                                                           260,
                                                                           270,115)];
        [BKSliderTitle setBackgroundColor:[UIColor clearColor]];
        [BKSliderTitle setTextAlignment:NSTextAlignmentCenter];
        [BKSliderTitle setNumberOfLines:5];
        [BKSliderTitle setTextColor:[UIColor colorWithRed:(217/255.f) green:(217/255.f) blue:(217/255.f) alpha:1]];
        [BKSliderTitle setText:[[self.BKSliderDataArray objectAtIndex:i] objectAtIndex:0]];
        [BKSliderTitle setAdjustsFontSizeToFitWidth:YES];
        
        [BKSliderPage addSubview:BKSliderImage];
        [BKSliderPage addSubview:BKSliderTitle];
        [self.BKSliderInfo addSubview:BKSliderPage];
    }
    
}


///////////////////////////////////////////
// ScrollView Delegate
///////////////////////////////////////////

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    if (!slideBeingUsed) {
        CGFloat pageWidth = self.BKSliderInfo.frame.size.width;
        int page = floor((self.BKSliderInfo.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        [self.BKSliderPaging setCurrentPage:page];
    }
}

- (void) PageControlChanged {
    CGRect frame;
    frame.origin.x = self.BKSliderInfo.frame.size.width * self.BKSliderPaging.currentPage;
    frame.origin.y = 0;
    frame.size = self.BKSliderInfo.frame.size;
    [self.BKSliderInfo scrollRectToVisible:frame animated:YES];
    slideBeingUsed = YES;
}

// move slider to the wanted slide
- (void) moveToWantedSlide:(int)index{
    CGRect frame;
    frame.origin.x = self.BKSliderInfo.frame.size.width * (self.BKSliderPaging.currentPage + (index));
    frame.origin.y = 0;
    frame.size = self.BKSliderInfo.frame.size;
    [self.BKSliderInfo scrollRectToVisible:frame animated:YES];
    slideBeingUsed = YES;
}


///////////////////////////////////////////
// ScrollView Delegate
///////////////////////////////////////////

// Accelero
- (void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
    
    // detecting Actions
    float roundX = floor(acceleration.x*10);
    
    if (roundX == -11 || roundX == -10) { isHorizontal = YES; }
    
    if(isHorizontal){
        if(roundX == -8 || roundX == -7){ [self playEaster:@"easter_egg"]; }
    }
}

- (void) playEaster:(NSString *)fName {
    [[UIAccelerometer sharedAccelerometer] setDelegate:nil];
    
    NSString *path  = [[NSBundle mainBundle] pathForResource:fName ofType:@"mp3"];
    if ([[NSFileManager defaultManager] fileExistsAtPath : path])
    {
        NSURL *pathURL = [NSURL fileURLWithPath : path];
        AudioServicesCreateSystemSoundID((CFURLRef) CFBridgingRetain(pathURL), &audioEffect);
        AudioServicesPlaySystemSound(audioEffect);
    }
    else
    {
        NSLog(@"error, file not found: %@", path);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
