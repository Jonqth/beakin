//
//  BKCatViewController.h
//  beakin
//
//  Created by Jonathan Araujo-Levy on 04/12/12.
//  Copyright (c) 2012 coesense. All rights reserved.
//

#import "BKViewController.h"
#import "BKInfoViewController.h"
#import "BKActionViewController.h"

@interface BKCatViewController : BKViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) BKInfoViewController *BKInfoViewController;
@property (strong, nonatomic) BKActionViewController *BKActionViewController;
@property (strong, nonatomic) UITableView *BKCategoryList;

@end
