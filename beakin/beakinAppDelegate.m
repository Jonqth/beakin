//
//  beakinAppDelegate.m
//  beakin
//
//  Created by Jonathan Araujo-Levy on 04/12/12.
//  Copyright (c) 2012 coesense. All rights reserved.
//

#import "beakinAppDelegate.h"
#import "BKCatViewController.h"

@implementation beakinAppDelegate

@synthesize BKCategoryViewController;
@synthesize BKNavigationController;

@synthesize userStackLike;
@synthesize userStackDislike;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor colorWithRed:(50/255.f) green:(50/255.f) blue:(50/255.f) alpha:1];
    [self.window makeKeyAndVisible];
    
    //Init root view
    self.BKCategoryViewController = [[BKCatViewController alloc] init];
    
    //Init navigation stack
    self.BKNavigationController = [[UINavigationController alloc] initWithRootViewController:self.BKCategoryViewController];
    [self.BKNavigationController setNavigationBarHidden:YES];
    
    self.window.rootViewController = self.BKNavigationController;
    
    //getting user data
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:@"/Users/parag/userData.plist"]){
        NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"userData" ofType:@"plist"];
        NSMutableDictionary *receivedUserData = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
        self.userStackLike = [receivedUserData objectForKey:@"likes"];
        self.userStackDislike = [receivedUserData objectForKey:@"dislikes"];
    }else{
        self.userStackLike = 0;
        self.userStackDislike = 0;
    }
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    
    // Saving user Data in plist
    NSMutableDictionary *userData = [[NSMutableDictionary alloc] init];
    [userData setObject:userStackLike forKey:@"likes"];
    [userData setObject:userStackDislike forKey:@"dislikes"];
    [userData writeToFile:@"/Users/parag/userData.plist" atomically:YES];
    
    id plist = [NSPropertyListSerialization dataFromPropertyList:(id)userData format:NSPropertyListXMLFormat_v1_0 errorDescription:nil];
}

@end
