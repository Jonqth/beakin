//
//  BKViewController.h
//  beakin
//
//  Created by Jonathan Araujo-Levy on 04/12/12.
//  Copyright (c) 2012 coesense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>

@class BKInfoViewController;

@interface BKViewController : UIViewController <UIAccelerometerDelegate>

// Navigation elements
@property (strong, nonatomic) UINavigationBar *BKNavgiationBar;
@property (strong, nonatomic) UINavigationItem *BKNavigationItem;

// Methods
- (void) presentView:(UIViewController *)incomingViewController;
- (void) dismissCurrentView;
- (void) setBackButton;

@end
