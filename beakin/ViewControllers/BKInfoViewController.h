//
//  BKInfoViewController.h
//  beakin
//
//  Created by Jonathan Araujo-Levy on 04/12/12.
//  Copyright (c) 2012 coesense. All rights reserved.
//

#import "BKViewController.h"

@interface BKInfoViewController : BKViewController <UIScrollViewDelegate>

@property (strong, nonatomic) UIScrollView *BKSliderInfo;
@property (strong, nonatomic) UIPageControl *BKSliderPaging;

@property (strong, nonatomic) NSMutableArray *BKSliderDataArray;

@end
