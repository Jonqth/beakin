//
//  beakinAppDelegate.h
//  beakin
//
//  Created by Jonathan Araujo-Levy on 04/12/12.
//  Copyright (c) 2012 coesense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BKCatViewController.h"

@class BKCatViewController;

@interface beakinAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) BKCatViewController *BKCategoryViewController;
@property (strong, nonatomic) UINavigationController *BKNavigationController;
@property (strong, nonatomic) NSNumber *userStackLike;
@property (strong, nonatomic) NSNumber *userStackDislike;

@end
