//
//  BKBeforeActionView.h
//  beakin
//
//  Created by Jonathan Araujo-Levy on 04/12/12.
//  Copyright (c) 2012 coesense. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BKBeforeActionView : UIView

@property (strong, nonatomic) UIViewController *parentViewController;
@property (strong, nonatomic) NSString *BKActionCatName;
@property (strong, nonatomic) NSString *BKActionSentenceLike;
@property (strong, nonatomic) NSString *BKActionSentenceDislike;
@property (strong, nonatomic) NSString *BKActionLikeSound;
@property (strong, nonatomic) NSString *BKActionDislikeSound;
@property (strong, nonatomic) NSString *BKBGActionImage;

- (id)initWithFrame:(CGRect)frame andActionData:(NSMutableArray *)actionData;

@end
