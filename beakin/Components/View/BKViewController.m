//
//  BKViewController.m
//  beakin
//
//  Created by Jonathan Araujo-Levy on 04/12/12.
//  Copyright (c) 2012 coesense. All rights reserved.
//

#import "BKViewController.h"
#import "BKInfoViewController.h"

@interface BKViewController ()

@end

@implementation BKViewController

// Navigation Elements
@synthesize BKNavgiationBar;
@synthesize BKNavigationItem;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.view setBackgroundColor:[UIColor colorWithRed:(50/255.f) green:(50/255.f) blue:(50/255.f) alpha:1]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    // Init UI
    [self mainUILoading];
}

// Loading Main UI (navBar...)

- (void) mainUILoading {
    [self setNavigationBar];
}


// Set UI elements

- (void) setNavigationBar {
    self.BKNavgiationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [self.BKNavgiationBar setBarStyle:UIBarMetricsDefault];
    [self.BKNavgiationBar setTintColor:[UIColor clearColor]];
    [self.BKNavgiationBar setBackgroundImage:[UIImage imageNamed:@"BK_navBar_bg.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIImageView *BKLogoNavView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BK_logoNavBar.png"]];
    self.BKNavigationItem = [[UINavigationItem alloc] init];
    [self.BKNavigationItem setTitleView:BKLogoNavView];
    [self.BKNavgiationBar setItems:[NSArray arrayWithObject:self.BKNavigationItem] animated:NO];
    
    [self.view addSubview:self.BKNavgiationBar];
}

- (void) setBackButton {
    
    // BackButton
    UIButton *BKBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *BKBackButtonImageNormal = [UIImage imageNamed:@"BK_backButtonNormal.png"];
    UIImage *BKBackButtonImageHighlighted = [UIImage imageNamed:@"BK_backButtonHighlighted.png"];
    [BKBackButton setFrame:CGRectMake(0, 0,BKBackButtonImageNormal.size.width, BKBackButtonImageNormal.size.height)];
    [BKBackButton setBackgroundImage:BKBackButtonImageNormal forState:UIControlStateNormal];
    [BKBackButton setBackgroundImage:BKBackButtonImageHighlighted forState:UIControlStateHighlighted];
    [BKBackButton addTarget:self action:@selector(switchToPrevious) forControlEvents:UIControlEventTouchUpInside];
    [self.BKNavgiationBar addSubview:BKBackButton];
}


// Methods to call views

- (void) presentView:(UIViewController *)incomingViewController {
    [self presentViewController:incomingViewController animated:YES completion:nil];
}

- (void) switchToPrevious {
     [self.navigationController popViewControllerAnimated:YES];
     [[UIAccelerometer sharedAccelerometer] setDelegate:nil];
}

- (void) dismissCurrentView {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
