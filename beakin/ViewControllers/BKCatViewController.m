//
//  BKCatViewController.m
//  beakin
//
//  Created by Jonathan Araujo-Levy on 04/12/12.
//  Copyright (c) 2012 coesense. All rights reserved.
//

#import "BKCatViewController.h"
#import "QuartzCore/QuartzCore.h"
#import "AFNetworking.h"

@interface BKCatViewController (){
 
    float rowHeight;
    NSArray *BKDataArray;
    int tableViewSizeHeight;
    
}
@end

@implementation BKCatViewController

// Views
@synthesize BKInfoViewController;
@synthesize BKActionViewController;

// Elements
@synthesize BKCategoryList;

// Arrays & Dictionnary

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {}
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    BKDataArray = [[NSArray alloc] init];
    NSURL *url = [[NSURL alloc] initWithString:@"http://glacial-falls-9240.herokuapp.com/categories.json"];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        BKDataArray = JSON;
        [self.BKCategoryList setHidden:NO];
        [self.BKCategoryList reloadData];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
    }];
    [operation start];
    [self catUILoading];
}

// Networking Data

- (void) setNetworkingData {
}

- (void) catUILoading {
    
    // InfoButton
    UIButton *BKInfoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *BKInfoButtonImageNormal = [UIImage imageNamed:@"BK_infoButtonNormal.png"];
    UIImage *BKInfoButtonImageHighlighted = [UIImage imageNamed:@"BK_infoButtonHighlighted.png"];
    [BKInfoButton setFrame:CGRectMake(self.BKNavgiationBar.frame.size.width-BKInfoButtonImageNormal.size.width, 0,
                                      BKInfoButtonImageNormal.size.width, BKInfoButtonImageNormal.size.height)];
    [BKInfoButton setBackgroundImage:BKInfoButtonImageNormal forState:UIControlStateNormal];
    [BKInfoButton setBackgroundImage:BKInfoButtonImageHighlighted forState:UIControlStateHighlighted];
    [BKInfoButton addTarget:self action:@selector(presentInfoView) forControlEvents:UIControlEventTouchUpInside];
    [self.BKNavgiationBar addSubview:BKInfoButton];
    
    tableViewSizeHeight = 416;
    // TableView
    if([[UIScreen mainScreen]bounds].size.height == 568){
        tableViewSizeHeight += 88;
    }
    
    self.BKCategoryList = [[UITableView alloc] initWithFrame:CGRectMake(0, self.BKNavgiationBar.frame.size.height, self.view.frame.size.width, tableViewSizeHeight)];
    [self.BKCategoryList setDelegate:self];
    [self.BKCategoryList setDataSource:self];
    [self.BKCategoryList setHidden:YES];
    [self.BKCategoryList setShowsVerticalScrollIndicator:NO];
    [self.BKCategoryList setAlwaysBounceVertical:NO];
    [self.BKCategoryList setBackgroundColor:[UIColor colorWithRed:(50/255.f) green:(50/255.f) blue:(50/255.f) alpha:1]];
    [self.BKCategoryList setSeparatorColor:[UIColor whiteColor]];
    [self.BKCategoryList setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:self.BKCategoryList];
    
}

- (void) presentInfoView {
    self.BKInfoViewController = [[BKInfoViewController alloc] init];
    [self presentView:self.BKInfoViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 /*
 ////////////////////////////////////////////////////////////////////////
 // TableView Methods
 */

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    self.BKActionViewController = [[BKActionViewController alloc] initWithData:[BKDataArray objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:self.BKActionViewController animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(BKDataArray && BKDataArray.count){
        return BKDataArray.count;
    }else{
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(BKDataArray && BKDataArray.count){
        UIImage *rowHeightSample = [UIImage imageNamed:[[BKDataArray objectAtIndex:0] objectForKey:@"thumbnail_img"]];
        return rowHeightSample.size.height;
    }else{
        return 1;
    }
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if(BKDataArray && BKDataArray.count){
    UIImageView *cellBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[[BKDataArray objectAtIndex:indexPath.row] objectForKey:@"thumbnail_img"]]];
        [cell setBackgroundView:cellBackground];
    }
    
    return cell;
}

 /*
 // TableView Methods
 ////////////////////////////////////////////////////////////////////////
 */


@end
