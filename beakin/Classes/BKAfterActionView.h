//
//  BKAfterActionView.h
//  beakin
//
//  Created by Jonathan Araujo-Levy on 04/12/12.
//  Copyright (c) 2012 coesense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "beakinAppDelegate.h"

@interface BKAfterActionView : UIView

@property (strong, nonatomic) beakinAppDelegate *sharedAppDelegate;

@end
